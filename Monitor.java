import java.util.Random;


public class Monitor {
    Random r = new Random();

    public synchronized void CS(int id) {
        System.out.println("[CS] " + id + " entered CS");
        Util.sleep(r.nextInt(1000));
        System.out.println("[CS] " + id + " exited CS");
    }
}
