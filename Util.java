public class Util {
    // method for sleep
    public static void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            System.out.println("Util.sleep: InterruptedException");
            Thread.currentThread().interrupt();
        }
    }

    // method for wait
    public static void wait(Object obj) {
        try {
            obj.wait();
        } catch (InterruptedException e) {
            System.out.println("Util.wait: InterruptedException");
            Thread.currentThread().interrupt();
        }
    }
}
