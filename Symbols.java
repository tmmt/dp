public class Symbols {
    public static final int Infinity = -1;
    public static final String nameServer = "localhost";
    public static final int serverPort = 8080;
    public static final int coordinator = 0;
    public static final int roundTime = 500;
    public static final boolean debugFlag = false;
}
