import java.util.*;


public class LockTester extends Thread {
    int myId;
    int iterations;
    Lock lock;
    Stack<Integer> stack;
    Random r = new Random();

    public LockTester(int id, Lock lock, Stack<Integer> stack, int iterations) {
        myId = id;
        this.lock = lock;
        this.stack = stack;
        this.iterations = iterations;
    }

    void printStackAndExit() {
        System.out.println("Critical section conflict: " + myId + " vs. " + stack.peek());
        System.out.println("Stack: " + stack);
        System.exit(0);
    }

    void nonCriticalSection() {
        System.out.println(myId + " is not in CS");
        Util.sleep(r.nextInt(1000));
    }

    void aCriticalSection() {
        if (!stack.isEmpty())
            printStackAndExit();

        stack.push(myId);

        System.out.println("[CS] " + myId + " entered CS");
        Util.sleep(r.nextInt(1000));

        if ((int) stack.peek() != myId)
            printStackAndExit();

        stack.pop();
        System.out.println("[CS] " + myId + " exited CS");
    }

    public void run() {
        for (int i = 0; i < iterations; ++i) {
            lock.requestCS(myId);
            aCriticalSection();
            lock.releaseCS(myId);
            nonCriticalSection();
        }
    }

    public static void test(int N, String lockName, int iterations) {
        LockTester t[];
        t = new LockTester[N];
        Lock lock;
        Stack<Integer> stack = new Stack<Integer>();

        switch (lockName) {
            case "bakery":
                lock = new Bakery(N);
                break;
            case "binary":
                lock = new BinarySemaphore();
                break;
            default:
                System.out.println("Lock choice not available: " + lockName);
                System.out.println("Using bakery");
                lock = new Bakery(N);
                break;
        }

        for (int i = 0; i < N; ++i) {
            t[i] = new LockTester(i, lock, stack, iterations);
            t[i].start();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("usage: java LockTester number_of_threads lock_type number_of_iterations");
            System.exit(1);
        }

        int N = Integer.parseInt(args[0]);
        String lockName = args[1];
        int iterations = Integer.parseInt(args[2]);

        LockTester.test(N, lockName, iterations);
    }
}
