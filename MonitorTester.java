import java.util.Random;


public class MonitorTester extends Thread {
    int myId;
    int iterations;
    Monitor monitor;
    Random r = new Random();

    public MonitorTester(int id, Monitor monitor, int iterations) {
        myId = id;
        this.monitor = monitor;
        this.iterations = iterations;
    }

    void nonCriticalSection() {
        System.out.println(myId + " is not in CS");
        Util.sleep(r.nextInt(1000));
    }

    public void run() {
        for (int i = 0; i < iterations; ++i) {
            monitor.CS(myId);
            nonCriticalSection();
        }
    }

    public static void test(int N, int iterations) {
        MonitorTester t[];
        t = new MonitorTester[N];
        Monitor monitor = new Monitor();

        for (int i = 0; i < N; ++i) {
            t[i] = new MonitorTester(i, monitor, iterations);
            t[i].start();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("usage: java MonitorTester number_of_threads number_of_iterations");
            System.exit(1);
        }

        int N = Integer.parseInt(args[0]);
        int iterations = Integer.parseInt(args[1]);

        MonitorTester.test(N, iterations);
    }
}
