import java.util.Random;


public class BoundedBufferTester extends Thread {
    int myId;
    int iterations;
    BoundedBufferMonitor monitor;
    String type;
    Random r = new Random();

    public BoundedBufferTester(int id, String type, BoundedBufferMonitor monitor, int iterations) {
        myId = id;
        this.monitor = monitor;
        this.iterations = iterations;
        this.type = type;
    }

    void nonCriticalSection(double value) {
        if (value != -1) {
            System.out.println(myId + " is outside deposit()");
        } else {
            System.out.println(myId + " is outside fetch()");
        }

        Util.sleep(r.nextInt(1000));
    }

    public void run() {
        double value = -1;

        for (int i = 0; i < iterations; ++i) {
            if (this.type == "deposit") {
                System.out.println("[DEPOSIT] " + myId + " has entered");
                monitor.deposit(myId);
                System.out.println("[DEPOSIT] " + myId + " has exited");
            } else {
                System.out.println("[FETCH] " + myId + " has entered");
                value = monitor.fetch();
                System.out.println("[FETCH] " + myId + " has exited");
            }

            nonCriticalSection(value);
        }
    }

    public static void test(int M, int N, int iterations) {
        BoundedBufferTester t[];
        t = new BoundedBufferTester[M + N];
        BoundedBufferMonitor monitor = new BoundedBufferMonitor();

        for (int i = 0; i < M; ++i) {
            t[i] = new BoundedBufferTester(i, "deposit", monitor, iterations);
            t[i].start();
        }

        for (int i = 0; i < N; ++i) {
            t[M + i] = new BoundedBufferTester(M + i, "fetch", monitor, iterations);
            t[M + i].start();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("usage: java BoundedBufferTester number_of_deposit_and_fetch_threads number_of_iterations");
            System.exit(1);
        }

        int N = Integer.parseInt(args[0]);
        int iterations = Integer.parseInt(args[1]);

        BoundedBufferTester.test(N, N, iterations);
    }
}
