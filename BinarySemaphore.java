public class BinarySemaphore implements Lock {
    boolean allowed = true;

    public synchronized void requestCS(int id) {
        while (!allowed)
            Util.wait(this);
        allowed = false;
    }

    public synchronized void releaseCS(int id) {
        allowed = true;
        notifyAll();
    }
}
