# Međusobno isključivanje i sinkronizacija u paralelnim sustavima

Osim u distribuiranim sustavima, problemi međusobnog isključivanja odnosno
sinkronizacije pojavljuju se i u paralelnim sustavima. Npr. u procesu koji se
sastoji od više dretvi potrebno je osigurati da se u svakom trenutku najviše
jedna dretva nalazi u svojoj kritičnoj sekciji. Ili je potrebno izvršiti neku
drugu vrstu sinkronizacije dretvi. Takvi problemi u paralelnim sustavima
rješavaju se algoritmima koji se zasnivaju na korištenju zajedničke memorije.
Programski jezik Java raspolaže i posebnim ugrađenim mehanizmom za
sinkronizaciju, takozvanim monitorom. U projektu je potrebno opisati,
implementirati i testirati barem dva algoritma za međusobno isključivanje ili
sinkronizaciju dretvi. Jedan od tih algoritama treba koristiti monitor, drugi
treba raditi bez monitora.

Uz izvorni kod može se vidjeti i [prezentacija.pdf](https://tmmt.gitlab.io/pdfs/dp/presentation.pdf "mutex and synchronization presentation") te [seminar.pdf](https://tmmt.gitlab.io/pdfs/dp/paper.pdf "mmutex and synchronization paper").

***
### Lock Tester

##### Lamport's Bakery Algorithm
```sh
javac LockTester.java && javac LockTester 3 bakery 100
```

##### Binary Semaphore
```sh
javac LockTester.java && javac LockTester 3 binary 100
```
***

### Monitor Tester
##### Simple Monitor Mutex
```sh
javac MonitorTester.java && javac MonitorTester 3 100
```
***

### Bounded Buffer Monitor Tester
```sh
javac BoundedBufferTester.java && javac BoundedBufferTester 3 100
```
***
